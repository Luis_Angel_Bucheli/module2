$(function(){
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();
	$('.carousel').carousel({
		interval:1800
	});

	$('#sugerencias').on('show.bs.modal', function (e) {
    console.log('Se va a mostrar el modal');
    $('#btnSugerencias').attr('disabled', true).removeClass('btn-info').addClass('btn-warning');
	});
	$('#sugerencias').on('shown.bs.modal', function (e) {
    console.log('El modal se muestra actualmente');
	});
	$('#sugerencias').on('hide.bs.modal', function (e) {
    console.log('El modal se va a ocultar');
    $('#btnSugerencias').attr('disabled', false).removeClass('btn-warning').addClass('btn-info');
	});
	$('#sugerencias').on('hidden.bs.modal', function (e) {
    console.log('El modal se ocultó');
	});
});